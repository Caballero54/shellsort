//codigo para c++

#include <iostream>
#include <stdio.h>
#include <iomanip>

using namespace std;
int main ()
{
  float notas[5], aux;
  int i = 0;
  int a, b, c, d, e;
  float suma = 0, mat = 0, leng = 0, geo = 0, ing = 0, lit = 0, media;

  for (i = 0; i < 5; i++)
    {
      cout << "alumno" << i + 1 << "nota1: ";
      cin >> a;
      cout << "alumno" << i + 1 << "nota2: ";
      cin >> b;
      cout << "alumno" << i + 1 << "nota3: ";
      cin >> c;
      cout << "alumno" << i + 1 << "nota4: ";
      cin >> d;
      cout << "alumno" << i + 1 << "nota5: ";
      cin >> e;

      notas[i] = (a + b + c + d + e) / 5;
      mat = (mat + a) / 2;
      leng = (leng + b) / 2;
      geo = (geo + c) / 2;
      ing = (ing + d) / 2;
      lit = (lit + e) / 2;

    }

  for (i = 0; i < 5; i++)
    suma = suma + notas[i];
  media = suma / 5;

  cout << fixed << setprecision (2);
  cout << endl << endl<< "Calificacion media del curso " << media <<endl ;
  cout << endl << endl<< "Calificacion Matematicas " << mat << endl;
  cout << endl << endl << "Calificacion Lenguaje " << leng <<endl;
  cout <<endl << endl << "Calificacion Geometria " << geo << endl;
  cout << endl << endl << "Calificacion Ingles " << ing << endl;
  cout << endl << endl << "Calificacion Literatura " << lit << endl;
  cout << "Alumnos segun su calificacion" << endl;

  for (i = 0; i < 5; i++)
    for (int j = 0; j < 4 - i; j++)
      {
	if (notas[i] > notas[i + 1])
	  {
	    aux = notas[i];
	    notas[i] = notas[i + 1];
	    notas[i + 1] = aux;
	    cout << "Alumno numero " << setw (3) << i + 1;
	    cout << "calificacion final " << aux << endl;
	  }
      }
  cout << endl;
  system("pause");
}